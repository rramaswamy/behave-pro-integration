
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes.Name;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
public class features_import {
	
	static LaxRedirectStrategy redirectStrategy = new LaxRedirectStrategy();
	static CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(redirectStrategy).build();
	
	static String Xacpt; 
	static Integer featureID = 0 ;
	static  String acpttoken;
	static String username ="TVijay-consultant";
	static String password ="atdd123#";
	static String name;
	static String payload;
	static String FeatureID;
	
	
	public static void main(String[] args) throws IOException {
	
		String FileName = System.getProperty("user.dir")+"\\features\\test-rco.feature";
		System.out.println(FileName);
	//	login_post(username,password);
	//	Xacpt=cookieid();
		
	//	getFeatureID("OTBPM - User Roles");
		upload_post(FileName);
			
		//delete_features("OTBPM - User Roles");
		//String change_file = "{\"name\":\"test-rco\"}";
		//String rename_payload = "{\"name\":\""+source_files[i].getName().replaceAll(".feature", "")+"\"}";
		//rename_file(56,change_file);
		
	}
	
	public static void login_post(String username,String password){	
		
	    String loginpayload ="username="+username+"&password="+password+"&dest-url=%2Fplugins%2Fservlet%2Fac%2Fpro.behave.hosted%2Fspecification-page%3Fproject.key%3DSAN%26project.id%3D10001&xsrf=";	
		
		try {
	
				//DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost postRequest = new HttpPost(
					"https://schl-atdd.atlassian.net/login");
				
				StringEntity input = new StringEntity(loginpayload);
				input.setContentType("application/x-www-form-urlencoded");
				postRequest.setEntity(input);
	
				HttpResponse response = client.execute(postRequest);
	
		//		System.out.println(response.getStatusLine().getStatusCode());
				
	
			  } catch (Exception e) {
	
				e.printStackTrace();
	
			  } 
		}
	
		
		public static Integer getFeatureID(String name) throws HttpResponseException, IOException{
	//String USER_AGENT = "X-acpt:080986f2fb0cd14d13cce248fff8f187547279721a9d633ec91d6587b94a6863fe4bde1794e02474cce633ab4e9cb01bf43f3d12103bc7570599cd28346307fe030b3571d4917e4dfdd5f368bfa88b4487c3d980bd69fae0a2b47bdd3d576e1e81aeb16dbed230c3338bed10d4987380846df6add0167b691dcf70b6f65c923523bc2b083d2b86d63b27ba915e831ac8";
			
			String url = "https://www.behave.pro/atlassian/connect/REST/1.0/project/10001/features";
			
			//DefaultHttpClient httpClient = new DefaultHttpClient();
			
			HttpGet getRequest = new HttpGet(url);
				
			getRequest.addHeader("accept", "application/json");
			getRequest.setHeader("X-acpt", Xacpt );

			HttpResponse response = client.execute(getRequest);
			
			int resp_code = response.getStatusLine().getStatusCode();
			String responseString = new BasicResponseHandler().handleResponse(response);
			System.out.println(responseString);
			System.out.println("getFeatures - " + resp_code);
			
			responseString = responseString.replace("{\"features\":[{", "");
			//System.out.println(responseString);
			responseString = responseString.replaceAll("\"available_filters\"(.*)", "");
			// System.out.println(responseString);
			responseString = responseString.replaceAll("\\}],", ""); 
			// System.out.println(responseString);
			
			 String[] res = responseString.split("(\\},\\{)");
				 // System.out.println(res.length);
				 for(int i = 0;i<res.length;i++){
					// System.out.println(res[i]);
					 if(!res[i].startsWith("\\{")){
						  res[i] = "{"+res[i];
						// System.out.println("{"+res[i]);
					 if(!res[i].endsWith("\\}")){
						  res[i]=res[i]+"}";
						  System.out.println(res[i]);
						  
				JSONObject jObject = new JSONObject(res[i]);
				if(jObject.getString("name").toLowerCase().contains(name.toLowerCase())){
					featureID = jObject.getInt("id");
					System.out.println(featureID);
							}
						}
						
						
						EntityUtils.consume(response.getEntity());
						
				  }
				  }
				return featureID;
				
		}

		public static Integer delete_features(String name) throws IOException{

	
			featureID = getFeatureID(name);
			
			System.out.println("Deleting feature ID - " + featureID);
			
			String url = "https://www.behave.pro/atlassian/connect/REST/1.0/project/10001/feature/"+featureID;
			try{
		
					//DefaultHttpClient httpClient = new DefaultHttpClient();
				 HttpDelete deleteRequest = new HttpDelete(url);
						
				 deleteRequest.addHeader("accept", "application/json");
				 deleteRequest.setHeader("X-acpt",Xacpt );
		
					HttpResponse response = client.execute(deleteRequest);
		
					System.out.println(response.getStatusLine().getStatusCode());
					System.out.println(response.getStatusLine().getReasonPhrase());
			}catch(Exception e){
				
			}
			return featureID;

		}


		public static String upload_post(String FileName) throws ClientProtocolException, IOException{	
//	String loginpayload ="X-acpt: 080986f2fb0cd14d13cce248fff8f187547279721a9d633ec91d6587b94a6863fe4bde1794e02474cce633ab4e9cb01bf43f3d12103bc7570599cd28346307fe030b3571d4917e4dfdd5f368bfa88b4487c3d980bd69fae0a2b47bdd3d576e1e81aeb16dbed230c3338bed10d4987380846df6add0167b691dcf70b6f65c923523bc2b083d2b86d63b27ba915e831ac8";	
			login_post("TVijay-consultant", "atdd123#");
			Xacpt=cookieid();
			
			HttpPost upload = new HttpPost(
				"https://www.behave.pro/atlassian/connect/REST/1.0/project/10001/upload");
			
			StringEntity input = new StringEntity("");
			input.setContentType("multipart/form-data");
			upload.setEntity(input);
			
			upload.setHeader("User-Agent", "Mozilla/5.0");
	//		upload.setHeader("XACPT", Xacpt);
			upload.setHeader("X-acpt", Xacpt );
			
			HttpEntity requestEntity = MultipartEntityBuilder.create()
			        .addBinaryBody("feature", new File(FileName)).build();
			        

			upload.setEntity(requestEntity);
			HttpResponse response = client.execute(upload);
			
			int resp_code = response.getStatusLine().getStatusCode();
			String resp = new BasicResponseHandler().handleResponse(response);
			System.out.println("uploading file - " + FileName);
			
			EntityUtils.consume(response.getEntity());
			return resp;
		
		}

		public static String cookieid() throws ClientProtocolException, IOException{
			String url = "https://schl-atdd.atlassian.net/plugins/servlet/ac/pro.behave.hosted/specification-page?project.key=SAN&project.id=10001";
	 
			//DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(url);
			getRequest.setHeader("User-Agent", "Mozilla/5.0");
			
			HttpResponse response = client.execute(getRequest);
			String tokenURL = null;
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = ""; 
			while ((line = rd.readLine()) != null) {	
				if(line.contains("www.behave.pro/atlassian")) {
				  tokenURL =  line.trim().replaceAll("\"src\":\"", "").replaceAll("\",", "");
		//		  System.out.println(tokenURL);
			  }
			}
		  
			 getRequest = new HttpGet(tokenURL);
			 getRequest.setHeader("User-Agent", "Mozilla/5.0");
				
			 response = client.execute(getRequest);
			 String acpttoken = null;
	 
			  rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			  
			  while((line = rd.readLine()) != null){
				if (line.contains("meta name=\"acpt\"")) {
					acpttoken = line.replaceAll("<meta name=\"acpt\" content=\"", "").replaceAll("\">", "");
					System.out.println("new token generated - " + acpttoken.trim());		
				}
			  }
	 
	 
	 return acpttoken;
	 
	}

	public static void rename_file(Integer FeatureID,String payload) throws ClientProtocolException, IOException{	
		
		 	
  		String url = "https://www.behave.pro/atlassian/connect/REST/2.0/project/10001/feature/"+FeatureID;

			//DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPut putRequest = new HttpPut(url);
			putRequest.setHeader("X-acpt", Xacpt );
			
			System.out.println("payload - " + payload);
			
			
			StringEntity input = new StringEntity(payload);
			input.setContentType("application/json");
			putRequest.setEntity(input);
			
			HttpResponse response = client.execute(putRequest);

			System.out.println("renaming file to actual file name instead of feature name");
	//		String resp = new BasicResponseHandler().handleResponse(response);
	//		System.out.println("uploadFile1 - " + resp);
		}
	}