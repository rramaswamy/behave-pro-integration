import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.MarionetteDriver;

import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;
import cucumber.runtime.PendingException;
import io.github.bonigarcia.wdm.MarionetteDriverManager;

public class stepdef {
	private WebDriver driver;
	private WebElement element;
	private String username;
	private String failed_msg;
	@Given("^user navigates to the website$")
	public void user_navigates_to_the_website() throws Throwable {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("chrome.switches","--disable-extensions");
		System.setProperty("webdriver.chrome.driver", "C:\\support\\Softwares\\chromedriver.exe");
		driver = new ChromeDriver(options);
		//	System.setProperty("webdriver.gecko.driver", "C:\\support\\Softwares\\geckodriver.exe");
		//MarionetteDriverManager.getInstance().setup();
		// driver = new MarionetteDriver();
		driver.get("http://www.gmail.com");
	}
	@When("^user enters his userid$")
	public void user_enters_his_userid() throws Throwable {
		element = driver.findElement(By.xpath("//*[@name='Email']"));
		element.sendKeys("testbooks37");
	    
	}
	@Then("^user clicks on the next button$")
	public void user_clicks_on_the_next_button() throws Throwable {
	    driver.findElement(ById.id("next")).click();
	    Thread.sleep(2000L);
	
	
	}


	@Then("^user enters his password$")
	public void user_enters_his_password() throws Throwable {
	   driver.findElement(By.xpath("//*[@name='Passwd']")).sendKeys("testbooks1");
		Thread.sleep(1000L);
	    
	}

	@Then("^user sign in and verify the username is present$")
	public void user_sign_in_and_verify_the_username_is_present() throws Throwable {
		driver.findElement(By.xpath("//*[@id='signIn']")).click();
		driver.findElement(By.xpath("//*[@id='gb']/div[1]/div[1]/div[2]/div[4]/div[1]/a/span")).click();
		username=driver.findElement(By.xpath("//*[@aria-label='Account Information']")).getText();
		username.contains("testtest");
		Thread.sleep(2000L);
	   
	 
	}	
		@Then("^user clicks on signout$")
		public void user_clicks_on_signout() throws Throwable {
			//driver.findElement(By.xpath("//*[@id='gb']/div[1]/div[1]/div[2]/div[4]/div[1]/a/span")).click();
			Thread.sleep(3000L);
			driver.findElement(By.xpath("//*[@id='gb_71']")).click();
			driver.close();
		}
			
			@Given("^User navigates to the website$")
			public void User_navigates_to_the_website() throws Throwable {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("chrome.switches","--disable-extensions");
				System.setProperty("webdriver.chrome.driver", "C:\\support\\Softwares\\chromedriver.exe");
				driver = new ChromeDriver(options);  
				driver.get("http://www.scholastic.com");
			}

			@When("^User enters email-id and the password$")
			public void User_enters_email_id_and_the_password() throws Throwable {
				
				driver.findElement(By.xpath("//*[@id='universal-account']/a[1]")).click();
				
				driver.switchTo().frame(0);
				Thread.sleep(2000L);
				
				driver.findElement(By.xpath("//*[@id='loginId2']")).sendKeys("test1gmail.com");
				driver.findElement(By.xpath("//*[@id='password2']")).sendKeys("test123");
				driver.findElement(By.xpath("//*[@id='nextbutton']")).click();
			}
			@Then("^User fails to login$")
			public void User_fails_to_login() throws Throwable {
			 failed_msg= driver.findElement(By.xpath("//*[@id='SIGN_IN_USER_INVALID']")).getText() ;
			 failed_msg.contains("The email or password you entered is incorrect.") ;
			 driver.close();
			}
			
	
			
	}


