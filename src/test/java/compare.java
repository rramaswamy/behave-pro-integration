import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import org.json.JSONObject;

import com.google.common.base.Charsets;
import com.google.common.io.Files;



public class compare extends diff_match_patch {
	
	
	public static void main(String[] args) throws IOException {

		diff_match_patch dmp = new diff_match_patch();
		features_import fi = new features_import();
		
		File source_path = new File(System.getProperty("user.dir")+"\\features\\");
		File target_path = new File(System.getProperty("user.dir")+"\\target\\generated-test-sources\\cucumber\\");
		File html_Path = new File(System.getProperty("user.dir")+"\\compare.html"); 
		  
		File[] source_files = source_path.listFiles();
		File[] target_files = target_path.listFiles();
		 	
		FileWriter fw = new FileWriter(html_Path);
   		BufferedWriter bw = new BufferedWriter(fw);
   		  	
   		for (int i = 0; i < source_files.length; i++) {
   		  		
   			String file1=null, file2=null;
   		  	
   			File f= new File("");
   			file1 = System.getProperty("user.dir")+"\\features\\"+((source_files[i].getName()));
   			
   			for (int n = 0; n < target_files.length; n++) {
   	    			
   				if (target_files[n].getName().contains(source_files[i].getName())){
   					file2 = System.getProperty("user.dir")+"\\target\\generated-test-sources\\cucumber\\"+target_files[n].getName();
   					
   					f = new File(file2);
   	    		}
   	    	}

   			if(f.exists()) {
   				String text1 = Files.toString(new File(file1), Charsets.UTF_8);
   			//	System.out.println(text1);
   				
   				String text2 = Files.toString(new File(file2), Charsets.UTF_8);
   			//	System.out.println(text2);
   				
 			//dmp.diff_main(text1, text2);
   			   bw.write("<font color=\"red\"><b><br>"+file1+"</b></font></br>");
   			    bw.write(dmp.diff_prettyHtml(dmp.diff_main(text1,text2)));
   	            bw.write("<br>#############################################</br>");
     			System.out.println(dmp.diff_main(text1,text2));
   			
   				  	           
/*   		   final LinesToCharsResult lines = dmp.diff_linesToChars(text1, text2);
   	           final LinkedList<Diff> diffs = dmp.diff_main(lines.chars1, lines.chars2);

   	           dmp.diff_charsToLines(diffs, lines.lineArray);

   	           System.out.println(dmp.diff_prettyHtml(diffs));
   	           System.out.println(diffs);
   	           bw.write("<br>"+file1+"</br>");
   	              
   	           bw.write(dmp.diff_prettyHtml(diffs));
   	           bw.write("<br>#############################################</br>");
*/
   	            
   			}else{
		         			
   				System.out.println("This file doesnot exist in behave pro ----- "+ source_files[i].getName());
     			bw.write("<font color=\"red\">This file doesnot exist in behave pro ^^^^^ "+ source_files[i].getName().replaceAll("\\d+-", "")+"</font>");
     			bw.write("<br>---------------------------------------------</br>" );
     			System.out.println("\n#######################################################\n");
     			
/*     			String upload = "fail";
     			JSONObject jObject;
     			int featureID = 0;
     			
     			try{
     				upload = fi.upload_post(file1);
     				jObject = new JSONObject(upload);
     				featureID = jObject.getInt("id");
     			}catch(Exception e){}
         			
         		if(upload.contains("\"id\"")){
	         		String rename_payload = "{\"name\":\""+source_files[i].getName().replaceAll(".feature", "")+"\"}";
	         		fi.rename_file(featureID,rename_payload);
         		}else{
         			System.out.println("upload failed");
         		}
*/         	}
		}
    	bw.close();		     
	}
}